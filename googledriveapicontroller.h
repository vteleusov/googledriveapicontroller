#ifndef GOOGLEDRIVEAPICONTROLLER_H
#define GOOGLEDRIVEAPICONTROLLER_H

#include <QObject>

#include <QtCore>
#include <QtNetwork>
#include <QOAuth2AuthorizationCodeFlow>

#include <functional>
#include <iostream>
using namespace std;

class FileData
{
public:
    QString kind;
    QString id;
    QString name;
    QString mimeType;
    FileData(QString _kind, QString _id, QString _name, QString _mimeType):
        kind(_kind),
        id(_id),
        name(_name),
        mimeType (_mimeType)
    {

    };
};

class GoogleDriveApiController : public QObject
{
    Q_OBJECT
public:
    explicit GoogleDriveApiController(QObject *parent = nullptr);
    GoogleDriveApiController(const QString &clientIdentifier, const QString &clientIdentifierSecretKey, QObject *parent = nullptr);

    bool isPermanent() const;
    void setPermanent(bool value);

    void aboutInfo(function<void(QNetworkReply::NetworkError error,
                                 QString errorString,
                                 QString userDisplayName,
                                 QString userEmailAddress,
                                 QString rootFolderID)> callBack);

    void fileInfo(QString fileID,
                  function<void(QNetworkReply::NetworkError error,
                                QString errorString,
                                QString iconLink,
                                QString title,
                                QString mimeType,
                                bool isFolder)> callBack);

    void folderContent(QString folderID,
                       function<void(QNetworkReply::NetworkError error,
                                     QString errorString,
                                     QList <QString>filesIds)> callBack);

    void parents(QString fileID,
                 function<void(QNetworkReply::NetworkError error,
                               QString errorString,
                               QList <QString>filesIds)> callBack);

    void downloadFile(QString fileID,
                      QString mimeType,
                      QString pathToSave,
                      function<void(QNetworkReply::NetworkError error,
                                    QString errorString)> callBack);

    void uploadFile(QString mimeType,
                    QString pathToFileToUpload,
                    QString fileNameWithWhichToUpload,
                    QString folderIdInWhichShouldUploadFile,
                    function<void(QNetworkReply::NetworkError error,
                                  QString errorString,
                                  QString fileID)> callBack);

    void searchFilesByName(QString fileNameString,
                           function<void(QNetworkReply::NetworkError error,
                                         QString errorString,
                                         QList <FileData> list)> callBack);

    void searchFilesInFolder(QString folderID,
                             function<void(QNetworkReply::NetworkError error,
                                           QString errorString,
                                           QList <FileData> list)> callBack);

private:
    QFile *localFileToUpload;
    QString pathToSaveDownloadedFile;
    function<void(QNetworkReply::NetworkError error,
                  QString errorString,
                  QString userDisplayName,
                  QString userEmailAddress,
                  QString rootFolderID)> aboutPrivateCallBack;
    function<void(QNetworkReply::NetworkError error,
                  QString errorString,
                  QString iconLink,
                  QString title,
                  QString mimeType,
                  bool isFolder)> fileInfoPrivateCallBack;
    function<void(QNetworkReply::NetworkError error,
                  QString errorString,
                  QList <QString>filesIds)> folderContentPrivateCallBack;
    function<void(QNetworkReply::NetworkError error,
                  QString errorString,
                  QList <QString>filesIds)> parentsPrivateCallBack;
    function<void(QNetworkReply::NetworkError error,
                  QString errorString)> downloadFilePrivateCallBack;
    function<void(QNetworkReply::NetworkError error,
                  QString errorString,
                  QString fileID)> uploadFilePrivateCallBack;
    function<void(QNetworkReply::NetworkError error,
                  QString errorString,
                  QList <FileData> list)> searchFilesByNamePrivateCallBack;
    function<void(QNetworkReply::NetworkError error,
                  QString errorString,
                  QList <FileData> list)> searchFilesInFolderPrivateCallBack;


    void test_AboutInfo();
    void test_FileInfo();
    void test_FolderContent();
    void test_Parents();
    void test_DownloadFile();
    void test_UploadFile();
    void test_SearchFilesByName();
    void test_SearchFilesInFolder();

public slots:
    void grant();
    void test();// for v.teleusov@keepsolid.com account on Google Drive.
    void generalTest();

private slots:
    void aboutPrivateSlot(QNetworkReply *reply);
    void fileInfoPrivateSlot(QNetworkReply *reply);
    void folderContentPrivateSlot(QNetworkReply *reply);
    void parentsPrivateSlot(QNetworkReply *reply);
    void downloadFilePrivateSlot(QNetworkReply *reply);
    void uploadFilePrivateSlot(QNetworkReply *reply);
    void searchFilesByNamePrivateSlot(QNetworkReply *reply);
    void searchFilesInFolderPrivateSlot(QNetworkReply *reply);

signals:
    void authenticated();
    void subscribed(const QUrl &url);

private:
    QOAuth2AuthorizationCodeFlow oauth2;
    bool permanent = false;
};

#endif // GOOGLEDRIVEAPICONTROLLER_H
