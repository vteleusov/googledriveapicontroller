 #include "googledriveapicontroller.h"

#include <QtGui>
#include <QtCore>
#include <QtNetworkAuth>


GoogleDriveApiController::GoogleDriveApiController(QObject *parent) : QObject(parent)
{
    auto replyHandler = new QOAuthHttpServerReplyHandler(1337, this);
    oauth2.setReplyHandler(replyHandler);
    oauth2.setAuthorizationUrl(QUrl("https://accounts.google.com/o/oauth2/auth"));
    oauth2.setAccessTokenUrl(QUrl("https://accounts.google.com/o/oauth2/token"));
    oauth2.setScope("https://www.googleapis.com/auth/drive");

    connect(&oauth2, &QOAuth2AuthorizationCodeFlow::statusChanged, [=](
            QAbstractOAuth::Status status) {
        if (status == QAbstractOAuth::Status::Granted)
        {
            emit authenticated();
            qDebug() << "authenticated()";
            qDebug() << oauth2.token();
            //this->test();
        }
    });

    oauth2.setModifyParametersFunction([&](QAbstractOAuth::Stage stage, QVariantMap *parameters) {
        if (stage == QAbstractOAuth::Stage::RequestingAuthorization && isPermanent())
            parameters->insert("duration", "permanent");
    });

    connect(&oauth2, &QOAuth2AuthorizationCodeFlow::authorizeWithBrowser,
            &QDesktopServices::openUrl);

    connect(this, &GoogleDriveApiController::authenticated, this, &GoogleDriveApiController::test);
}

GoogleDriveApiController::GoogleDriveApiController(const QString &clientIdentifier, const QString &clientIdentifierSecretKey, QObject *parent) : GoogleDriveApiController(parent)
{
    oauth2.setClientIdentifier(clientIdentifier);
    oauth2.setClientIdentifierSharedKey(clientIdentifierSecretKey);
}

bool GoogleDriveApiController::isPermanent() const
{
    return permanent;
}

void GoogleDriveApiController::setPermanent(bool value)
{
    permanent = value;
}

void GoogleDriveApiController::grant()
{
    oauth2.grant();
}

void GoogleDriveApiController::aboutInfo(function<void(QNetworkReply::NetworkError error,
                                                       QString errorString,
                                                       QString userDisplayName,
                                                       QString userEmailAddress,
                                                       QString rootFolderID)> callBack)
{
    // https://developers.google.com/drive/v2/reference/about/get?authuser=1

    oauth2.get(QUrl("https://www.googleapis.com/drive/v2/about"));
    aboutPrivateCallBack = callBack;
    connect(oauth2.networkAccessManager(), &QNetworkAccessManager::finished, this, aboutPrivateSlot);
    //*/

    /*
    oauth2.get(QUrl("https://www.googleapis.com/drive/v2/about"));

    connect(oauth2.networkAccessManager(), &QNetworkAccessManager::finished, [=](QNetworkReply* reply) {

        if (reply->error() == QNetworkReply::NoError)
        {
            QString json = (QString)reply->readAll();
            json = json.replace("\n", "");

            QJsonDocument document = QJsonDocument::fromJson(json.toUtf8());
            Q_ASSERT(document.isObject());
            const auto rootObject = document.object();
            Q_ASSERT(rootObject.value("kind").toString() == "drive#about");
            QString rootFolderID = rootObject["rootFolderId"].toString();
            const auto userObject= rootObject["user"].toObject();
            QString userDisplayName = userObject["displayName"].toString();
            QString userEmailAddress = userObject["emailAddress"].toString();

            callBack(reply->error(), reply->errorString(), userDisplayName, userEmailAddress, rootFolderID);
        }
        else
        {
            callBack(reply->error(), reply->errorString(), nullptr, nullptr, nullptr);
        }
    });
    //*/
}

void GoogleDriveApiController::fileInfo(QString fileID,
                                        function<void(QNetworkReply::NetworkError error,
                                                      QString errorString,
                                                      QString iconLink,
                                                      QString title,
                                                      QString mimeType,
                                                      bool isFolder)> callBack)
{
    //https://developers.google.com/drive/v2/reference/files/get?authuser=1

    QString urlString = "https://www.googleapis.com/drive/v2/files/" + fileID;
    oauth2.get(QUrl(urlString));
    fileInfoPrivateCallBack = callBack;
    connect(oauth2.networkAccessManager(), &QNetworkAccessManager::finished, this, fileInfoPrivateSlot);
}

void GoogleDriveApiController::folderContent(QString folderID,
                                             function<void(QNetworkReply::NetworkError error,
                                                           QString errorString,
                                                           QList <QString>filesIds)> callBack)
{
    //https://developers.google.com/drive/v2/reference/children/list?authuser=1

    QString urlString = "https://www.googleapis.com/drive/v2/files/" + folderID + "/children";
    oauth2.get(QUrl(urlString));

    folderContentPrivateCallBack = callBack;
    connect(oauth2.networkAccessManager(), &QNetworkAccessManager::finished, this, folderContentPrivateSlot);
}

void GoogleDriveApiController::parents(QString fileID,
                                       function<void(QNetworkReply::NetworkError error,
                                                     QString errorString,
                                                     QList <QString>filesIds)> callBack)
{
    //https://developers.google.com/drive/v2/reference/parents/list?authuser=1

    QString urlString = "https://www.googleapis.com/drive/v2/files/" + fileID + "/parents";
    oauth2.get(QUrl(urlString));

    parentsPrivateCallBack = callBack;
    connect(oauth2.networkAccessManager(), &QNetworkAccessManager::finished, this, parentsPrivateSlot);
}

void GoogleDriveApiController::downloadFile(QString fileID,
                                            QString mimeType,
                                            QString pathToSave,
                                            function<void(QNetworkReply::NetworkError error,
                                                          QString errorString)> callBack)
{
    //https://developers.google.com/drive/v3/web/manage-downloads

    QVariantMap map;
    QString altParameterValue = "media";
    map["alt"] = QVariant(altParameterValue.toUtf8());

    QString urlString = "https://www.googleapis.com/drive/v3/files/" + fileID;
    oauth2.get(QUrl(urlString), map);

    pathToSaveDownloadedFile = pathToSave;
    downloadFilePrivateCallBack = callBack;
    connect(oauth2.networkAccessManager(), &QNetworkAccessManager::finished, this, downloadFilePrivateSlot);
}

void GoogleDriveApiController::uploadFile(QString mimeType,
                                          QString pathToFileToUpload,
                                          QString fileNameWithWhichToUpload,
                                          QString folderIdInWhichShouldUploadFile,
                                          function<void(QNetworkReply::NetworkError error,
                                                        QString errorString,
                                                        QString fileID)> callBack)
{
    auto multi_part = new QHttpMultiPart{ QHttpMultiPart::FormDataType };
    auto localFileToUpload_part = new QHttpPart{};
    auto localFileToUpload_part_name = new QHttpPart{};
    localFileToUpload = new QFile{ pathToFileToUpload.toUtf8() };

    if(!localFileToUpload->exists())
    {
        qDebug() << " file does not exist.";
        return;
    }
    else
        qDebug() << "exist!";

    //if (!localFileToUpload->open(QIODevice::ReadWrite)) { return; }
    if (!localFileToUpload->open(QIODevice::ReadOnly)) { return; }


    localFileToUpload_part_name->setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json"));
    localFileToUpload_part_name->setBody("{\"name\": \"" + fileNameWithWhichToUpload.toUtf8() + "\", \"parents\": [\"" + folderIdInWhichShouldUploadFile.toUtf8() + "\"]}");


    localFileToUpload_part->setHeader(QNetworkRequest::ContentTypeHeader, QVariant(mimeType.toUtf8()));
    localFileToUpload_part->setBodyDevice(localFileToUpload);

    multi_part->append(*localFileToUpload_part_name);
    multi_part->append(*localFileToUpload_part);

    QUrl url = QUrl("https://www.googleapis.com/upload/drive/v3/files");
    QNetworkRequest req(url);
    const QString bearer_format = QStringLiteral("Bearer %1");
    const QString bearer = bearer_format.arg(oauth2.token());
    req.setRawHeader("Authorization", bearer.toUtf8());

    //--------------------------------------------------------------------
    uploadFilePrivateCallBack = callBack;
    connect(oauth2.networkAccessManager(), &QNetworkAccessManager::finished, this, uploadFilePrivateSlot);
    oauth2.networkAccessManager()->post(req, multi_part);
}

void GoogleDriveApiController::searchFilesByName(QString fileNameString,
                                           function<void(QNetworkReply::NetworkError error,
                                                         QString errorString,
                                                         QList <FileData> list)> callBack)
{
    //https://stackoverflow.com/questions/24720075/how-to-get-list-of-files-by-folder-on-google-drive-api
    //https://developers.google.com/drive/v3/web/search-parameters


    QVariantMap map;
    map["q"] = QVariant("name contains '" + fileNameString.toUtf8() + "'");

    oauth2.get(QUrl("https://www.googleapis.com/drive/v3/files"), map);

    searchFilesByNamePrivateCallBack = callBack;
    connect(oauth2.networkAccessManager(), &QNetworkAccessManager::finished, this, searchFilesByNamePrivateSlot);
}

void GoogleDriveApiController::searchFilesInFolder(QString folderID,
                                                   function<void(QNetworkReply::NetworkError error,
                                                                 QString errorString,
                                                                 QList <FileData> list)> callBack)
{
    //https://stackoverflow.com/questions/24720075/how-to-get-list-of-files-by-folder-on-google-drive-api
    //https://developers.google.com/drive/v3/web/search-parameters


    QVariantMap map;
    map["q"] = QVariant("'" + folderID.toUtf8() + "' in parents");
    oauth2.get(QUrl("https://www.googleapis.com/drive/v3/files"), map);

    searchFilesInFolderPrivateCallBack = callBack;
    connect(oauth2.networkAccessManager(),
            &QNetworkAccessManager::finished,
            this,
            searchFilesInFolderPrivateSlot);
}

void GoogleDriveApiController::test()
{
    //this->test_AboutInfo(); // Done!
    //this->test_FileInfo();
    //this->test_FolderContent(); // Done!
    //this->test_Parents();
    //this->test_DownloadFile();
    //this->test_UploadFile();
    //this->test_SearchFilesByName();
    //this->test_SearchFilesInFolder();
    //*/

    this->generalTest();
}

void GoogleDriveApiController::generalTest()
{
    QString fileNameToUpload = "testPDFFileToUploadAndDownloadFromGoogleDrive.pdf";
    //QString pathToFileToUpload = "C:\\Users\\user\\Downloads\\" + fileNameToUpload;
    QString pathToFileToUpload = ":\\Resources\\" + fileNameToUpload;
    QString fileNameToSaveUploadedFileInGoogleDrive = "file_to_download_after_uploading.pdf";
    QString fileNameToSaveUploadedFromGoogleDriveFileInHDD = "file_downloaedd_from_googleDrive_after_uploading.pdf";
    QString pathToSaveFileAfterDownloadFromGoogleDrive = "C:\\Users\\user\\Downloads\\" + fileNameToSaveUploadedFromGoogleDriveFileInHDD;

    aboutInfo([this, pathToFileToUpload, fileNameToSaveUploadedFileInGoogleDrive, pathToSaveFileAfterDownloadFromGoogleDrive](QNetworkReply::NetworkError error,
              QString errorString,
              QString userDisplayName,
              QString userEmailAddress,
              QString rootFolderID)
    {
        if (error == QNetworkReply::NoError)
        {
            qDebug() << "aboutInfo() test: success!";
            qDebug() << "userDisplayName: " << userDisplayName;
            qDebug() << "userEmailAddress: " << userEmailAddress;
            qDebug() << "rootFolderID: " << rootFolderID;

            searchFilesInFolder(rootFolderID.toUtf8(),
                                [this, pathToFileToUpload, fileNameToSaveUploadedFileInGoogleDrive, pathToSaveFileAfterDownloadFromGoogleDrive, rootFolderID](QNetworkReply::NetworkError error,
                                QString errorString,
                                QList <FileData> list)
            {
                if (error == QNetworkReply::NoError)
                {
                    qDebug() << "searchFilesInFolder() test: success!";
                    qDebug() << "Files count: " << list.count();
                    int i = 0;

                    foreach (const FileData & fileData, list) {

                        QString kind = fileData.kind;
                        QString id = fileData.id;
                        QString name = fileData.name;
                        QString mimeType = fileData.mimeType;

                        qDebug() << ++i << ".";
                        qDebug() << "kind: " << kind;
                        qDebug() << "id: " << id;
                        qDebug() << "name: " << name;
                        qDebug() << "mimeType: " << mimeType;
                    }
                }
                else
                {
                    qDebug() << "searchFilesInFolder() test: failure with error: ";
                    qDebug() << errorString;
                }

                uploadFile("application/pdf",
                           pathToFileToUpload,
                           fileNameToSaveUploadedFileInGoogleDrive,
                           rootFolderID,
                           [this, pathToFileToUpload, fileNameToSaveUploadedFileInGoogleDrive, pathToSaveFileAfterDownloadFromGoogleDrive, rootFolderID](QNetworkReply::NetworkError error, QString errorString, QString fileID)
                {
                    if (error == QNetworkReply::NoError)
                    {
                        qDebug() << "uploadFile() test: success!";
                        qDebug() << "uploadedFileID: " << fileID;

                        QString uploadedFileID = fileID;

                        fileInfo(uploadedFileID,
                                 [this, pathToFileToUpload, fileNameToSaveUploadedFileInGoogleDrive, pathToSaveFileAfterDownloadFromGoogleDrive, rootFolderID, uploadedFileID](QNetworkReply::NetworkError error,
                                 QString errorString,
                                 QString iconLink,
                                 QString title,
                                 QString mimeType,
                                 bool isFolder)
                        {
                            if (error == QNetworkReply::NoError)
                            {
                                qDebug() << "fileInfo() test: success!";

                                qDebug() << "iconLink: " << iconLink;
                                qDebug() << "title: " << title;
                                qDebug() << "mimeType: " << mimeType;
                                qDebug() << "isFolder: " << (isFolder ? "true" : "false");

                                QString uploadedFileMimeType = mimeType;

                                parents(uploadedFileID,
                                        [this, pathToFileToUpload, fileNameToSaveUploadedFileInGoogleDrive, pathToSaveFileAfterDownloadFromGoogleDrive, rootFolderID, uploadedFileID, uploadedFileMimeType](QNetworkReply::NetworkError error,
                                        QString errorString,
                                        QList <QString>filesIds)
                                {
                                    if (error == QNetworkReply::NoError)
                                    {
                                        qDebug() << "parents() test: success!";
                                        qDebug() << "Parents count: " << filesIds.count();

                                        int i = 0;
                                        foreach (const QString value, filesIds)
                                        {
                                            qDebug() << ++i << ". parentID: " << value;
                                        }

                                        searchFilesByName(fileNameToSaveUploadedFileInGoogleDrive,
                                                          [this, pathToFileToUpload, fileNameToSaveUploadedFileInGoogleDrive, pathToSaveFileAfterDownloadFromGoogleDrive, rootFolderID, uploadedFileID, uploadedFileMimeType](QNetworkReply::NetworkError error,
                                                          QString errorString,
                                                          QList <FileData> list)
                                        {
                                            if (error == QNetworkReply::NoError)
                                            {
                                                qDebug() << "searchFilesByName() test: success!";
                                                qDebug() << "Files count: " << list.count();
                                                int i = 0;

                                                foreach (const FileData & fileData, list) {

                                                    QString kind = fileData.kind;
                                                    QString id = fileData.id;
                                                    QString name = fileData.name;
                                                    QString mimeType = fileData.mimeType;

                                                    qDebug() << ++i << ".";
                                                    qDebug() << "kind: " << kind;
                                                    qDebug() << "id: " << id;
                                                    qDebug() << "name: " << name;
                                                    qDebug() << "mimeType: " << mimeType;
                                                }

                                                downloadFile(uploadedFileID,
                                                             uploadedFileMimeType,
                                                             pathToSaveFileAfterDownloadFromGoogleDrive,
                                                             [](QNetworkReply::NetworkError error, QString errorString)
                                                {
                                                    if (error == QNetworkReply::NoError)
                                                    {
                                                        qDebug() << "downloadFile() test: success!";
                                                    }
                                                    else
                                                    {
                                                        qDebug() << "downloadFile() test: failure with error: ";
                                                        qDebug() << errorString;
                                                    }
                                                });
                                            }
                                            else
                                            {
                                                qDebug() << "searchFilesByName() test: failure with error: ";
                                                qDebug() << errorString;
                                            }
                                        });
                                    }
                                    else
                                    {
                                        qDebug() << "parents() test: failure with error: ";
                                        qDebug() << errorString;
                                    }
                                });
                            }
                            else
                            {
                                qDebug() << "fileInfo() test: failure with error: ";
                                qDebug() << errorString;
                            }

                        });
                    }
                    else
                    {
                        qDebug() << "uploadFile() test: failure with error: ";
                        qDebug() << errorString;
                    }
                });




            });
        }
        else
        {
            qDebug() << "aboutInfo() test: failure with error: ";
            qDebug() << errorString;
        }
    });
}

void GoogleDriveApiController::test_SearchFilesInFolder()
{
    searchFilesInFolder("0AAfT7R58HfijUk9PVA",
                        [](QNetworkReply::NetworkError error,
                        QString errorString,
                        QList <FileData> list)
    {
        if (error == QNetworkReply::NoError)
        {
            qDebug() << "searchFilesInFolder() test: success!";
            qDebug() << "Files count: " << list.count();
            int i = 0;

            foreach (const FileData & fileData, list) {

                QString kind = fileData.kind;
                QString id = fileData.id;
                QString name = fileData.name;
                QString mimeType = fileData.mimeType;

                qDebug() << ++i << ".";
                qDebug() << "kind: " << kind;
                qDebug() << "id: " << id;
                qDebug() << "name: " << name;
                qDebug() << "mimeType: " << mimeType;
            }
        }
        else
        {
            qDebug() << "searchFilesInFolder() test: failure with error: ";
            qDebug() << errorString;
        }
    });
}

void GoogleDriveApiController::test_UploadFile()
{
    uploadFile("application/pdf",
               "C:\\Users\\user\\Downloads\\DownloadedFile_2.pdf",
               "file_name_2.pdf",
               "0AAfT7R58HfijUk9PVA",
               [](QNetworkReply::NetworkError error, QString errorString, QString fileID)
    {
        if (error == QNetworkReply::NoError)
        {
            qDebug() << "uploadFile() test: success!";
            qDebug() << "fileID: " << fileID;
        }
        else
        {
            qDebug() << "uploadFile() test: failure with error: ";
            qDebug() << errorString;
        }
    });
}

void GoogleDriveApiController::test_SearchFilesByName()
{
    searchFilesByName("file",
                      [](QNetworkReply::NetworkError error,
                      QString errorString,
                      QList <FileData> list)
    {
        if (error == QNetworkReply::NoError)
        {
            qDebug() << "searchFilesByName() test: success!";
            qDebug() << "Files count: " << list.count();
            int i = 0;

            foreach (const FileData & fileData, list) {

                QString kind = fileData.kind;
                QString id = fileData.id;
                QString name = fileData.name;
                QString mimeType = fileData.mimeType;

                qDebug() << ++i << ".";
                qDebug() << "kind: " << kind;
                qDebug() << "id: " << id;
                qDebug() << "name: " << name;
                qDebug() << "mimeType: " << mimeType;
            }
        }
        else
        {
            qDebug() << "searchFilesByName() test: failure with error: ";
            qDebug() << errorString;
        }
    });
}

void GoogleDriveApiController::test_DownloadFile()
{
    downloadFile("1QxoFXcglZXS5eCclfwGtlXllWky-AwpfEtmicm64QX0",
                 "application/pdf",
                 "C:\\Users\\user\\Downloads\\DownloadedFile_2.pdf",
                 [](QNetworkReply::NetworkError error, QString errorString)
    {

        if (error == QNetworkReply::NoError)
        {
            qDebug() << "downloadFile() test: success!";
        }
        else
        {
            qDebug() << "downloadFile() test: failure with error: ";
            qDebug() << errorString;
        }
    });
}

void GoogleDriveApiController::test_Parents()
{
    parents("0BwfT7R58HfijWnYtN20tZjRCOFE",
            [](QNetworkReply::NetworkError error,
            QString errorString,
            QList <QString>filesIds)
    {
        if (error == QNetworkReply::NoError)
        {
            qDebug() << "parents() test: success!";

            int i = 0;
            foreach (const QString value, filesIds)
            {
                qDebug() << ++i << ". fileID: " << value;
            }
        }
        else
        {
            qDebug() << "parents() test: failure with error: ";
            qDebug() << errorString;
        }
    });
}

void GoogleDriveApiController::test_FolderContent()
{
    folderContent("0AAfT7R58HfijUk9PVA",
                  [](QNetworkReply::NetworkError error,
                  QString errorString,
                  QList <QString>filesIds)
    {
        if (error == QNetworkReply::NoError)
        {
            qDebug() << "folderContent() test: success!";

            int i = 0;
            foreach (const QString value, filesIds)
            {
                qDebug() << ++i << ". fileID: " << value;
            }
        }
        else
        {
            qDebug() << "folderContent() test: failure with error: ";
            qDebug() << errorString;
        }
    });
}

void GoogleDriveApiController::test_AboutInfo()
{
    aboutInfo([](QNetworkReply::NetworkError error,
              QString errorString,
              QString userDisplayName,
              QString userEmailAddress,
              QString rootFolderID)
    {
        if (error == QNetworkReply::NoError)
        {
            qDebug() << "aboutInfo() test: success!";
            qDebug() << "userDisplayName: " << userDisplayName;
            qDebug() << "userEmailAddress: " << userEmailAddress;
            qDebug() << "rootFolderID: " << rootFolderID;
        }
        else
        {
            qDebug() << "aboutInfo() test: failure with error: ";
            qDebug() << errorString;
        }
    });
}

void GoogleDriveApiController::test_FileInfo()
{
    fileInfo("0AAfT7R58HfijUk9PVA",
             [](QNetworkReply::NetworkError error,
             QString errorString,
             QString iconLink,
             QString title,
             QString mimeType,
             bool isFolder)
    {
        if (error == QNetworkReply::NoError)
        {
            qDebug() << "fileInfo() test: success!";
            qDebug() << "iconLink: " << iconLink;
            qDebug() << "title: " << title;
            qDebug() << "mimeType: " << mimeType;
            qDebug() << "isFolder: " << ((isFolder == true) ? "true" : "false");
        }
        else
        {
            qDebug() << "fileInfo() test: failure with error: ";
            qDebug() << errorString.toUtf8();
        }
    });
}

/*
 private slots:
//*/

void GoogleDriveApiController::aboutPrivateSlot(QNetworkReply *reply)
{
    disconnect(oauth2.networkAccessManager(),
               &QNetworkAccessManager::finished,
               this,
               aboutPrivateSlot);

    if (reply->error() == QNetworkReply::NoError)
    {
        QString json = (QString)reply->readAll();
        json = json.replace("\n", "");

        QJsonDocument document = QJsonDocument::fromJson(json.toUtf8());
        Q_ASSERT(document.isObject());
        const auto rootObject = document.object();
        Q_ASSERT(rootObject.value("kind").toString() == "drive#about");
        QString rootFolderID = rootObject["rootFolderId"].toString();
        const auto userObject= rootObject["user"].toObject();
        QString userDisplayName = userObject["displayName"].toString();
        QString userEmailAddress = userObject["emailAddress"].toString();

        aboutPrivateCallBack(reply->error(), reply->errorString(), userDisplayName, userEmailAddress, rootFolderID);
    }
    else
    {
        aboutPrivateCallBack(reply->error(), reply->errorString(), nullptr, nullptr, nullptr);
    }
}

void GoogleDriveApiController::fileInfoPrivateSlot(QNetworkReply *reply)
{
    disconnect(oauth2.networkAccessManager(),
               &QNetworkAccessManager::finished,
               this,
               fileInfoPrivateSlot);

    if (reply->error() == QNetworkReply::NoError)
    {
        QString json = (QString)reply->readAll();
        json = json.replace("\n", "");

        QJsonDocument document = QJsonDocument::fromJson(json.toUtf8());
        Q_ASSERT(document.isObject());
        const auto rootObject = document.object();
        Q_ASSERT(rootObject.value("kind").toString() == "drive#file");
        QString iconLink = rootObject["iconLink"].toString();
        QString title = rootObject["title"].toString();
        QString mimeType = rootObject["mimeType"].toString();
        QString folderMimeType = "application/vnd.google-apps.folder";

        int result = QString::compare(mimeType, folderMimeType, Qt::CaseInsensitive);

        fileInfoPrivateCallBack(reply->error(), reply->errorString(), iconLink, title, mimeType, result == 0);
    }
    else
    {
        fileInfoPrivateCallBack(reply->error(), reply->errorString(), nullptr, nullptr, nullptr, false);
    }
}

void GoogleDriveApiController::folderContentPrivateSlot(QNetworkReply *reply)
{
    disconnect(oauth2.networkAccessManager(),
               &QNetworkAccessManager::finished,
               this,
               folderContentPrivateSlot);

    if (reply->error() == QNetworkReply::NoError)
    {
        QString json = (QString)reply->readAll();
        json = json.replace("\n", "");
        //qDebug() << json.toUtf8();

        QJsonDocument document = QJsonDocument::fromJson(json.toUtf8());
        Q_ASSERT(document.isObject());
        const auto rootObject = document.object();
        Q_ASSERT(rootObject.value("kind").toString() == "drive#childList");
        QJsonArray jsonArray = rootObject["items"].toArray();

        QList <QString>list = QList<QString>();

        foreach (const QJsonValue & value, jsonArray) {
            QJsonObject obj = value.toObject();
            QString id = obj["id"].toString();
            list.append(QString(id));
        }

        folderContentPrivateCallBack(reply->error(), reply->errorString(), list);
    }
    else
    {
        folderContentPrivateCallBack(reply->error(), reply->errorString(), QList<QString>());
    }
}

void GoogleDriveApiController::parentsPrivateSlot(QNetworkReply *reply)
{
    disconnect(oauth2.networkAccessManager(),
               &QNetworkAccessManager::finished,
               this,
               parentsPrivateSlot);

    if (reply->error() == QNetworkReply::NoError)
    {
        QString json = (QString)reply->readAll();
        json = json.replace("\n", "");

        QJsonDocument document = QJsonDocument::fromJson(json.toUtf8());
        Q_ASSERT(document.isObject());
        const auto rootObject = document.object();
        Q_ASSERT(rootObject.value("kind").toString() == "drive#parentList");
        QJsonArray jsonArray = rootObject["items"].toArray();

        QList <QString>list = QList<QString>();

        foreach (const QJsonValue & value, jsonArray) {
            QJsonObject obj = value.toObject();
            QString id = obj["id"].toString();
            list.append(QString(id));
        }

        parentsPrivateCallBack(reply->error(), reply->errorString(), list);
    }
    else
    {
        parentsPrivateCallBack(reply->error(), reply->errorString(), QList<QString>());
    }
}

void GoogleDriveApiController::downloadFilePrivateSlot(QNetworkReply *reply)
{
    if (reply->error() == QNetworkReply::NoError)
    {
        // Save the file
        QByteArray b = reply->readAll();
        QFile file(pathToSaveDownloadedFile);
        file.open(QIODevice::WriteOnly);
        QDataStream out(&file);
        out << b;
        reply->deleteLater();
        file.close();
        downloadFilePrivateCallBack(reply->error(), reply->errorString());
    }
    else
    {
        downloadFilePrivateCallBack(reply->error(), reply->errorString());
    }
}

void GoogleDriveApiController::uploadFilePrivateSlot(QNetworkReply *reply)
{
    disconnect(oauth2.networkAccessManager(),
               &QNetworkAccessManager::finished,
               this,
               uploadFilePrivateSlot);

    if (reply->error() == QNetworkReply::NoError)
    {
        QString json = (QString)reply->readAll();
        json = json.replace("\n", "");

        QJsonDocument document = QJsonDocument::fromJson(json.toUtf8());
        Q_ASSERT(document.isObject());
        const auto rootObject = document.object();
        QString fileID = rootObject["id"].toString();

        localFileToUpload->close();
        uploadFilePrivateCallBack(reply->error(), reply->errorString(), fileID);
    }
    else
    {
        uploadFilePrivateCallBack(reply->error(), reply->errorString(), QString());
    }
}

void GoogleDriveApiController::searchFilesByNamePrivateSlot(QNetworkReply *reply)
{
    disconnect(oauth2.networkAccessManager(),
               &QNetworkAccessManager::finished,
               this,
               searchFilesByNamePrivateSlot);

    if (reply->error() == QNetworkReply::NoError)
    {
        QString json = (QString)reply->readAll();
        json = json.replace("\n", "");

        QJsonDocument document = QJsonDocument::fromJson(json.toUtf8());
        Q_ASSERT(document.isObject());
        const auto rootObject = document.object();
        Q_ASSERT(rootObject.value("kind").toString() == "drive#fileList");
        QJsonArray jsonArray = rootObject["files"].toArray();

        int i = 0;
        QList <FileData> list = QList <FileData> ();

        foreach (const QJsonValue & value, jsonArray) {
            QJsonObject obj = value.toObject();

            QString kind = obj["kind"].toString();
            QString id = obj["id"].toString();
            QString name = obj["name"].toString();
            QString mimeType = obj["mimeType"].toString();

            list.append(FileData(kind, id, name, mimeType));
        }

        searchFilesByNamePrivateCallBack(reply->error(), reply->errorString(), list);
    }
    else
    {
        QList <FileData> list = QList <FileData> ();
        searchFilesByNamePrivateCallBack(reply->error(), reply->errorString(), list);
    }
}

void GoogleDriveApiController::searchFilesInFolderPrivateSlot(QNetworkReply *reply)
{
    disconnect(oauth2.networkAccessManager(),
               &QNetworkAccessManager::finished,
               this,
               searchFilesInFolderPrivateSlot);

    if (reply->error() == QNetworkReply::NoError)
    {
        QString json = (QString)reply->readAll();
        json = json.replace("\n", "");

        QJsonDocument document = QJsonDocument::fromJson(json.toUtf8());
        Q_ASSERT(document.isObject());
        const auto rootObject = document.object();
        Q_ASSERT(rootObject.value("kind").toString() == "drive#fileList");
        QJsonArray jsonArray = rootObject["files"].toArray();

        int i = 0;
        QList <FileData> list = QList <FileData> ();

        foreach (const QJsonValue & value, jsonArray) {
            QJsonObject obj = value.toObject();

            QString kind = obj["kind"].toString();
            QString id = obj["id"].toString();
            QString name = obj["name"].toString();
            QString mimeType = obj["mimeType"].toString();

            list.append(FileData(kind, id, name, mimeType));
        }

        searchFilesInFolderPrivateCallBack(reply->error(), reply->errorString(), list);
    }
    else
    {
        QList <FileData> list = QList <FileData> ();
        searchFilesInFolderPrivateCallBack(reply->error(), reply->errorString(), list);
    }
}
